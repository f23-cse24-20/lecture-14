#include <iostream>
using namespace std;

int main() {

    /*
        Ask user for 5 integers.
        Determine how many of them were even and how many were odd.
    */

    int evenCount = 0;
    int oddCount = 0;

    for (int i = 0; i < 5; i++){
        int x;
        cin >> x;

        if (x % 2 == 0){
            evenCount++;
            // evenCount = evenCount + 1; // evenCount++; // evenCount += 1;
        }
        else {
            oddCount++;
        }

    }

    if (evenCount == 1){
         cout << "There was " << evenCount << " even number." << endl;
    }
    else{
        cout << "There were " << evenCount << " even numbers." << endl;
    }
    
    if (oddCount == 1){
        cout << "There was " << oddCount << " odd number." << endl;
    }
    else{
        cout << "There were " << oddCount << " odd numbers." << endl;
    }
    

    return 0;
}