#include <iostream>
using namespace std;

int main() {

    /*
        Ask user for 5 integers.
        Classify each one as even or odd.
        Even numbers leave a remainder of 0 when divided by 2
    */
    
    for (int i = 0; i < 100; i++){
        int x;
        cin >> x;

        if (x % 2 == 0){
            cout << x << " is even." << endl;
        }
        else{
            cout << x << " is odd." << endl;
        }
    }


    return 0;
}